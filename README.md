# CE9010 Introduction to Data Analysis (AY18/19)
## Predicting the success rates of Kickstarter projects

Crowdfunding, the practice of financing a project, start-up venture, 
or an altruistic cause by raising capital from a large group of people ("crowd"),
has gained significant popularity amongst companies and individuals alike over the past decade.
According to crowdfunding.com, as of March 2019, $9.5 billion has been crowdfunded
across the three largest crowdfunding online platforms IndieGoGo, GoFundMe and
Kickstarter, backed by a total of 75 million individuals.
Clearly, not every campaign put forward on the platforms get successfully funded.

In this project, we are interested in studying what features of a 
project/campaign are most important in determining their success rates.
In particular, we will study a dataset consisting of Kickstarter projects and
build a classification model to predict the outcome of the campaign, given the
metadata of the project.

A large proportion of the dataset we require is downloaded from [webrobots.io's repository](https://webrobots.io/kickstarter-datasets/).
We also perform our own scraping to supplement the downloaded dataset.
The raw data is combined, cleaned, and then analyzed using well-known exploratory data analysis (EDA) techniques.
Finally, we compare and evaluate several classifiers for predicting the campaigns' successes.


## Information for Group Members

### Workflow

1. Set up a python virtual environment. Aside from the usual packages, please also pip install [nb-clean](https://github.com/srstevenson/nb-clean) and configure it to clean your notebook outputs before committing and pushing to the repository. This is to help prevent unnecessary push/pull/merge conflicts.
2. I would recommend also keeping a local backup of your notebook (outside of the CE9010_project git folder) once in a while just in case.
3. Always do a `git pull` before you start working locally (to minimize possibilities of merge conflicts).
4. If there are merge conflicts, you might find [`nbdime mergetool`](https://nbdime.readthedocs.io/en/latest/) helpful in resolving conflicts in the Jupyter notebook.


### Helpful Links

1. How to create reproducible results in Jupyter: https://jakevdp.github.io/blog/2017/03/03/reproducible-data-analysis-in-jupyter/
2. Resolving merge conflicts: https://swcarpentry.github.io/git-novice/09-conflict/index.html
